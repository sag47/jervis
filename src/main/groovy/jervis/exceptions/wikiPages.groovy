/*
   Copyright 2014-2015 Sam Gleske - https://github.com/samrocketman/jervis

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   */
package jervis.exceptions

/**
  A simple class, whose only purpose is to house static strings referencing the <a href="https://github.com/samrocketman/jervis/wiki" target="_blank">Jervis wiki</a>,
  to be used as helpful hints when throwing exceptions.
 */
class wikiPages {

    /**
      A static reference to the <a href="https://github.com/samrocketman/jervis/wiki/Supported-Languages" target="_blank">supported languages wiki page</a>.
     */
    public static String supported_languages = 'https://github.com/samrocketman/jervis/wiki/Supported-Languages'

    /**
      A static reference to the <a href="https://github.com/samrocketman/jervis/wiki/Supported-Tools" target="_blank">supported tools wiki page</a>.
     */
    public static String supported_tools = 'https://github.com/samrocketman/jervis/wiki/Supported-Tools'

    /**
      A static reference to the <a href="https://github.com/samrocketman/jervis/wiki/Specification-for-lifecycles-file" target="_blank">lifecycles file spec wiki page</a>.
     */
    public static String lifecycles_spec = 'https://github.com/samrocketman/jervis/wiki/Specification-for-lifecycles-file'

    /**
      A static reference to the <a href="https://github.com/samrocketman/jervis/wiki/Specification-for-toolchains-file" target="_blank">toolchains file spec wiki page</a>.
     */
    public static String toolchains_spec = 'https://github.com/samrocketman/jervis/wiki/Specification-for-toolchains-file'

    /**
      A static reference to the <a href="https://github.com/samrocketman/jervis/wiki/Specification-for-platforms-file" target="_blank">platforms file spec wiki page</a>.
     */
    public static String platforms_spec = 'https://github.com/samrocketman/jervis/wiki/Specification-for-platforms-file'
}
